FROM debian
RUN apt-get -qq update && apt-get -qq -y install curl xz-utils \
 && echo 'http 80/tcp' >> /etc/services \
 && echo 'https 443/tcp' >> /etc/services \
 && curl -L -o guix-binary.x86_64-linux.tar.xz \
         https://gitlab.com/samplet/build-guix-testing/-/jobs/artifacts/master/raw/guix-binary.x86_64-linux.tar.xz?job=build \
 && tar -C / -xf guix-binary.x86_64-linux.tar.xz \
 && rm guix-binary.x86_64-linux.tar.xz \
 && GUIX_PROFILE=/var/guix/profiles/per-user/root/current-guix \
 && echo "GUIX_PROFILE=$GUIX_PROFILE" >> ~/.bashrc \
 && echo 'export GUIX_PROFILE' >> ~/.bashrc \
 && echo '. $GUIX_PROFILE/etc/profile' >> ~/.bashrc \
 && groupadd --system guixbuild \
 && for i in $(seq -w 1 10); \
    do \
      useradd -g guixbuild -G guixbuild \
              -d /var/empty -s $(which nologin) \
              -c "Guix build user $i" --system  \
              guixbuilder$i; \
    done \
 && GUIX=$GUIX_PROFILE/bin/guix \
 && $GUIX archive --authorize < $GUIX_PROFILE/share/guix/hydra.gnu.org.pub \
 && $GUIX archive --authorize < $GUIX_PROFILE/share/guix/berlin.guixsd.org.pub
CMD . ~/.bashrc \
 && guix-daemon --build-users-group=guixbuild \
                --disable-chroot \
                --substitute-urls="https://mirror.hydra.gnu.org \
                                   https://ci.guix.info)" \
  & bash
